/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package employeelab10;
import java.util.*;
/**
 *
 * @author jaiminlakhani
 */
public class EmployeeTool {
    public boolean isPromotionDueThisYear(Employee employee, boolean goodPerformance) {
        Calendar dateJoined = Calendar.getInstance();
        dateJoined.setTime(employee.getEmployeeDateOfJoining());
        dateJoined.add(Calendar.YEAR,2);
        
        Calendar today = Calendar.getInstance();
        
       
        return today.after( dateJoined ) && goodPerformance;  
    }
    public double calcIncomeTaxForCurrentYear( Employee employee, double taxPercentage)
    {
        return employee.getEmployeeSalary() * taxPercentage;
    }
    
}
