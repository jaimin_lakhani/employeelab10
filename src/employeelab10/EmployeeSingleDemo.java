/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package employeelab10;
import java.util.*;
/**
 *
 * @author jaiminlakhani
 */
public class EmployeeSingleDemo {
    public static void main(String[]args) {
        Calendar dateJoin = Calendar.getInstance();
        dateJoin.set( 2021, 2 , 23);
        
        Employee employee = new Employee("2424", dateJoin.getTime() , 30000);
        
        EmployeeTool tool = new EmployeeTool();
        
        System.out.println(" PROMOTION  " +tool.isPromotionDueThisYear(employee, true));
        System.out.println("TAX " +tool.calcIncomeTaxForCurrentYear(employee, 0.68));
       
    }
}
