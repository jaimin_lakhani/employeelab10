/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package employeelab10;
import java.util.*;
/**
 *
 * @author jaiminlakhani
 */
public class Employee {
    private String employeeId;
    private Date dateOfJoining;
    private double Salary;

   
    public Employee(String employeeId, Date dateOfJoining, double Salary) {
        this.employeeId = employeeId;
        this.dateOfJoining = dateOfJoining;
        this.Salary = Salary;
    }

 
    public Date getEmployeeDateOfJoining() {
        return dateOfJoining;
    }
      public String getEmployeeId() {
        return employeeId;
    }
      public double getEmployeeSalary() {
          return Salary;
      }

    
 }
